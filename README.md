## Wiener Filter for Short-Reach Fiber-Optic Links


This repository provides the corresponding program code for the publication "Wiener Filter for Short Fiber-Optic Links", which was accepted as an IEEE Communications Letter. (A pre-print is also available [here](https://arxiv.org/abs/2004.12148)).


### Citation

The software is provided under the open-source [MIT license](https://opensource.org/licenses/MIT). If you use the software in your academic work, please cite the accompanying [document](https://doi.org/10.1109/LCOMM.2020.3006921) as follows:

> D. Plabst, F. J. G. Gómez, T. Wiegart and N. Hanik, "Wiener Filter for Short-Reach Fiber-Optic Links," in *IEEE Communications Letters*, vol. 24, no. 11, pp. 2546-2550, Nov. 2020, doi: 10.1109/LCOMM.2020.3006921.


The corresponding BibTeX entry is:
```
@ARTICLE{plabst2020wienerfilter,
  author={D. {Plabst} and F. J. G. {G{\'o}mez} and T. {Wiegart} and N. {Hanik}},
  journal={IEEE Communications Letters}, 
  title={Wiener Filter for Short-Reach Fiber-Optic Links}, 
  year={2020},
  volume={24},
  number={11},
  pages={2546-2550},
  doi={10.1109/LCOMM.2020.3006921}}
```
