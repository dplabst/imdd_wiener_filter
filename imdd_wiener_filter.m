% /*********************************************************************
%  * Publication: Wiener Filter for Short-Reach Fiber-Optic Links
%  * Copyright (c) 2020 Daniel Plabst <d.plabst@tum.de>
%  * Associate Professorship of Line Transmission Technology (LÜT)
%  * Institute for Communications Engineering (LNT)
%  * Technical University of Munich, Germany
%  * www.lnt.ei.tum.de 
%  * All rights reserved.
%  ********************************************************************/
clear;
addpath('helpers')

%% ----------------- System Settings -------------------
N_os = 2; %Oversampling factor: ONLY implemented for N_os=2
B    = 1; %Normalized bandwidth
Ts   = 1/B; %Normalized time-period 
Ts_prime = Ts/N_os;
K_sym = 100000; %Number of symbols to be transmitted

%% -------------- Fiber settings [Units] --------------
f_sym   = 27E9; %[Baud/s]
L_max   = 20E3; %[m] Maximum length of fiber  %Paper uses [m] 20E3 
L       = 0E3; %[m] Length of fiber 
f_s     = f_sym*N_os; %[Hz] f_s of ADC after SLD 
D       = 17*1E-12/(1E-9*1E3); %[s/ (m*m)]
c_light = 299792458; %[m/s]
lambda  = 1550E-9; %[m]
alpha_att = 0.046E-3; %[1/m]
gamma   = 1.27E-3; %[1/(W*m)]
beta_2  = -D*lambda^2/(2*pi*c_light); %[s^2/m]
P_tx_max = 0.1/(gamma*(1-exp(-alpha_att*L_max))/alpha_att); %[W]
P_tx_opt = min(0.1/(gamma*(1-exp(-alpha_att*L))/alpha_att), P_tx_max ); %[W] Use same transmit powers, even if L=0

%% ------------- Receiver settings ----------------
% Comparison device: Thorlabs RXM40AF: 40GHz Bandwidth Amplified Photoreceiver
% [URL]: https://www.thorlabs.com/_sd.cfm?fileName=TTN179570-S01.pdf&partNumber=RXM40AF
NEP_rx = 21E-12; %[W/sqrt(Hz)] 'Single-sided' noise equivalent power  [Agrawal, P. 154]
B_rx_rxm40 = 40E9; %[Hz]
B_rx_el    = min(B_rx_rxm40, f_sym); %[Hz] One-Sided Bandwidth after the g_rx brickwall filter 

%% ------------ Constellation and scaling ---------
gen_uniform_pmf = @(P_tx,D_norm,Q) P_tx-(D_norm*2*P_tx)/2+(0:Q-1)'/(Q-1)*(D_norm*2*P_tx); %Generate support for equally spaced PMF
Q               = 16; %Set size of uniform PAM modulation alphabet
mu_s_wo_sqrt    = P_tx_opt; %Mean before sqrt
t_alpha         = 1/(2*sqrt(P_tx_opt)); %First order Taylor series coefficient
t_beta          = sqrt(P_tx_opt)/2; %First order Taylor series coefficient
mu_s_tay        = t_alpha*mu_s_wo_sqrt+t_beta; %Mean after sqrt approximation

%% ------------- Pulseshaping TX/RX--------------
g_tx_kappa = (-400:400).'; %Multiples of sampling instance
g_tx = sqrt(1)*sinc(g_tx_kappa/N_os); %Unit energy pulseshaping filter

%% --------------- Dispersive channel-------------------
M_gtx       = length(g_tx_kappa);
df          = 1/M_gtx; %Frequency spacing
omega       = ifftshift( [-floor(M_gtx/2)*df:df:-floor(M_gtx/2)*df+(M_gtx-1)*df].' );  %Normalized frequency vector on interval [-0.5,0.5), ifftshift -> Matlab convention
c_disp      = (2*pi)^2*f_s^2*beta_2/2*L; %Lumped constant
H           = exp(-1i*omega.^2*c_disp);
psi_vec     = ifft(H.*fft(g_tx)); %Channel impulse response
idx_trunc   = find(abs(psi_vec)/max(abs(psi_vec)) > 0.01, 1); 
psi_vec     = psi_vec(idx_trunc:end-idx_trunc);%Truncate
M           = length(psi_vec); %Length of CIR
Psi         = sparse(convmtx( fliplr(psi_vec.'), 20*M)); %CIR matrix (limited max size to 20*M to save memory)
Psi_prime   = Psi(:,1:N_os:end); %Downsample CIR matrix columnwise

%%  ---------------  WF Settings ----------------
K=M; 
Kprime = floor((K-1)/N_os); 
Mprime = floor((M-1)/N_os); 
Nprime = Kprime+Mprime-1;
idx_Mprime  = Mprime+1; %Index of interest, 1-based indexing
Psi_WF = Psi_prime(1:K,1:Kprime+Mprime-1); %Downsampled matrix for Wiener Filtering 

N0_B_vec = 10.^(-(0:0.5:11)*10/10); %N0*B

m4_central = @(A) mean( (A-mean(A)).^4 ); %Fourth-order central moment 

%% -------------- Geometric Shaping -------------
parfor idx_n = 1:length(N0_B_vec) %Iterate over N0*B
    var_n = N0_B_vec(idx_n);
    
    ESR = @(D_norm) ... 
            parse_esr(Psi_WF, ...
            idx_Mprime,...
            t_alpha^2*mean(gen_uniform_pmf(P_tx_opt, D_norm, Q).^2-P_tx_opt.^2),... %var_s with sqrt approximation
            mu_s_tay,...
            var_n,...
            t_alpha,...
            t_beta, ...
            m4_central(t_alpha*gen_uniform_pmf(P_tx_opt, D_norm, Q)+t_beta) ... #Fourth order central moment with sqrt approximation
        );
      
    [D_norm_opt,min_ESR]  = fminbnd(ESR,0,1); %Optimize, vary D_norm from 0...1
    D_norm_opt_vec(idx_n) = D_norm_opt;
    min_ESR_vec(idx_n)    = min_ESR;
    
end

%% -------------------------- Simulate --------------------------
L_maxiter=10; %Number of realizations 
parfor idx_n = 1:length(N0_B_vec)
    var_n = N0_B_vec(idx_n); %N0*B
    S_equal = gen_uniform_pmf(P_tx_opt, D_norm_opt_vec(idx_n), Q);  %Generate SNR-optimized equally spaced constellation 
    mu_s_wo_sqrt    = mean(S_equal); %mean of constellation 
    var_s_wo_sqrt   = 1/Q * sum((S_equal-mu_s_wo_sqrt).^2); %variance of constellation 
    mu_s_tay        = t_alpha*mu_s_wo_sqrt+t_beta; %mean after sqrt approximation 
    var_s_tay       = t_alpha^2 * var_s_wo_sqrt; %variance after sqrt approximation 
    S_sqrt          = sqrt(S_equal); %Generate sqrt constellation 
    P_tx_opt_sanity = 1/Q * sum((S_sqrt).^2);
    
    %Sanity check: 
    assert(abs(P_tx_opt_sanity-P_tx_opt) < 2*eps,'Optical TX power constraint violated');
    
    %Wiener Filter with SQRT approximation, SNR dependent 
    [g,g_mean,~] = wiener_filter(Psi_WF, idx_Mprime, var_s_tay, mu_s_tay, var_n, t_alpha, t_beta, m4_central(t_alpha*S_equal+t_beta) );
    
    %Naive WF for linear channels       
    C_uu_naive = (var_s_wo_sqrt*(Psi_WF*Psi_WF') + var_n*eye(K));
    c_su_naive = (var_s_wo_sqrt * Psi_WF(:,idx_Mprime).');
    mu_u_naive = Psi_WF*ones(Nprime,1)*mu_s_wo_sqrt;
    g_naive = real(c_su_naive/C_uu_naive); %Discard imaginary parts
    g_mean_naive = real(mu_s_wo_sqrt - c_su_naive*(C_uu_naive\mu_u_naive)); %Discard imaginary parts
    
    %% Feasible SNR for a specific receiver
    var_n_rx_feasible = (NEP_rx)^2 * B_rx_el; %Noise power of specific receiver 
    alpha_ampl = exp(-alpha_att/2*L); %Attenuation for amplitude signals 
    
    %Analytic ESR: 
    ESR_WF_analytic(idx_n) = min_ESR_vec(idx_n); 
    rx_sig_vec_mag = 0; 
    rx_sig_vec_mag_atten = 0;
    for l = 1:L_maxiter %Monte Carlo Simulation 
        rng(l); 
        idx_tx = randi(Q,[ceil((K_sym+M-1)/N_os),1]);
        s = S_sqrt(idx_tx); %Generate Transmit Symbols
        
        %% Convolve data symbols with CIR 
        L_conv = length(s)*N_os+M-1; %Length of zeropadded FFT 
        s_conv = ifft(fft(psi_vec,L_conv).*fft(upsample(s,N_os),L_conv)); %Fast convolution, could also use fftfilt
        s_conv = s_conv(M:end-(M)); %Discard M-1 on both sides, to allow only fully convolved parts
        
        r_prime = abs(s_conv).^2; %Noise-free RX signal
        r_prime_atten = abs(alpha_ampl*s_conv).^2; %Attenuated noise-free RX signal 
        
        n = sqrt(var_n)*randn(K_sym,1); %AWGN, N0*B variance, zero-mean
        u = r_prime + n; %receive filter + noise addition 
        
        %% WF
        L_deconv = K_sym+K-1; %Deconvolution length 
        s_hat = ifft( fft(u, L_deconv) .* fft( fliplr(g).', L_deconv)) + g_mean; %Fast deconvolution, fliplr to express shifted inner products by convolution 
        s_hat = downsample( s_hat(K:end-(K-1)), N_os, 0); %Discard not-fulled overlapped parts of convolution and downsample

        %% WF Naive
        s_hat_naive = ifft( fft(u, L_deconv) .* fft( fliplr(g_naive).', L_deconv)) + g_mean_naive;
        s_hat_naive = downsample( s_hat_naive(K:end-(K-1)), N_os, 0); %Discard not-fulled overlapped parts of convolution and downsample
        s_tx  = s(idx_Mprime:idx_Mprime+length(s_hat)-1).^2; %TX symbols starting from s_ind and of length of estimates

        rx_sig_vec_mag       = rx_sig_vec_mag + abs(r_prime).^2;  %Absolute square of receive vector 
        rx_sig_vec_mag_atten = rx_sig_vec_mag_atten + abs(r_prime_atten).^2; %Absolute square of attenuated receive vector
        
        %% Calculate erros and rates
        ESR_WF(l,idx_n)       = mean((s_tx - s_hat).^2)/var_s_wo_sqrt;   
        ESR_WF_naive(l,idx_n) = mean((s_tx - s_hat_naive).^2)/var_s_wo_sqrt; 
        
        MI_WF(l,idx_n)        = mi_cg(S_equal.', s_hat.',       idx_tx(idx_Mprime:idx_Mprime+length(s_hat)-1).');
        MI_WF_naive(l,idx_n)  = mi_cg(S_equal.', s_hat_naive.', idx_tx(idx_Mprime:idx_Mprime+length(s_hat)-1).');

    end

    %% RX power 
    rx_power = sum(rx_sig_vec_mag/L_maxiter)/((floor((K_sym-1)/2) + floor((M-1)/2)+1)*N_os); %Numerically compute RX power
    rx_power_vec(idx_n) = rx_power; 
    
    rx_power_w_atten = sum(rx_sig_vec_mag_atten/L_maxiter)/((floor((K_sym-1)/2) + floor((M-1)/2)+1)*N_os); %Numerically compute RX power with attenuation
    SNR_feas(idx_n) = 10*log10(rx_power_w_atten/var_n_rx_feasible); %Feasible electrical SNR for a specific receiver 
    
end

%% -------------------- Plot ------------------
cmap        = colormap(lines);
cmap        = cmap(1:7,:);
cmap        = cmap(3,:);
SNR         = rx_power_vec./(N0_B_vec); 
SNR_dB      = 10*(log10(SNR));
C_awgn_real = 1/2*log2(1+SNR); 

%% ----------------- Achievable rates ------------
figure(1);
%clf(1)
hold on;
grid on; 
    plot(SNR_dB,mean(MI_WF,1)      ,'+-.','color',cmap);
    plot(SNR_dB,mean(MI_WF_naive,1),'o-.','color',cmap);
    plot(SNR_dB,C_awgn_real,'k-');
    max_feas_rx_snr = SNR_dB(SNR_dB <= SNR_feas);
    plot(max_feas_rx_snr(end)*[1 1],[0, C_awgn_real(length(max_feas_rx_snr)) ],'k--');
title('Achievable Rates')
legend('WF', 'WF naive', 'C AWGN (real)')
xlabel('$\mathrm{SNR}_\mathrm{el} = \frac{P_\mathrm{rx,el}}{\sigma_n^2}$','Interpreter','latex')
ylabel('bpcu')

%% -------------------- Plot ESR ----------------------------
figure(2);
%clf(2)
grid on;
hold on;
    plot(SNR_dB,10*log10(mean(ESR_WF,1))      ,'+-.','color',cmap);
    %
    plot(SNR_dB,10*log10(ESR_WF_analytic)      ,'s-.','color',cmap);
    plot(SNR_dB,10*log10(mean(ESR_WF_naive,1)) ,'o-.','color',cmap);
    plot(max_feas_rx_snr(end)*[1 1],[0, -30],'k--');
title('Error Power to Signal Power Ratio (ESR)')
legend('ESR Empirical (WF)','ESR Analytical (WF)', 'ESR Empirical (WF naive)')
xlabel('$\mathrm{SNR}_\mathrm{el} = \frac{P_\mathrm{rx,el}}{\sigma_n^2}$','Interpreter','latex')
ylabel('ESR')

%% -------------------- Geometric Shaping  -----------------------
figure(3);
%clf(3)
grid on;
hold on;
    plot(SNR_dB,D_norm_opt_vec,'^-.','color',cmap)
title('Geometric Shaping')
ylabel('$\frac{D}{2 P_\mathrm{tx,opt}}$','Interpreter','latex')
xlabel('$\mathrm{SNR}_\mathrm{el} = \frac{P_\mathrm{rx,el}}{\sigma_n^2}$','Interpreter','latex')
legend('Geom. shaping for WF')

