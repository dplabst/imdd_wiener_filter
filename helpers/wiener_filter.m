% /*********************************************************************
%  * Publication: Wiener Filter for Short-Reach Fiber-Optic Links
%  * Copyright (c) 2020 Daniel Plabst <d.plabst@tum.de>
%  * Institute for Communications Engineering (LNT)
%  * Technical University of Munich, Germany
%  * www.lnt.ei.tum.de 
%  ********************************************************************/

% This function calculates the Wiener Filter with (approximated) square-root predistortion 
function [g, g_mean, mse_analytic] = wiener_filter(Psi_prime, ...    % Channel matrix of the dispersive channel
                                              idx_Mprime,...        % Index in transmit data vector, which is to be estimated (M')
                                              var_s_tay,...         % Approximate transmit symbol variance (Taylor series) 
                                              mu_s_tay,...          % Approximate transmit symbol mean (Taylor series) 
                                              var_n,...             % Noise variance N0*B
                                              t_alpha,...           % Taylor series constant 
                                              t_beta,...            % Taylor series constant 
                                              M4)                   % Fourth order central moment with sqrt approximation

                                                  
Nprime = size(Psi_prime,2);
K      = size(Psi_prime,1); 
w      = Psi_prime*ones(Nprime,1);
Q      = abs(Psi_prime).^2; 

c_su = 2*t_alpha^(-1) * var_s_tay * mu_s_tay * real(Psi_prime(:,idx_Mprime) .* conj(w)).';
mu_u = real(diag(var_s_tay * (Psi_prime*Psi_prime')) + mu_s_tay^2 * abs(w).^2 );
C_uu = real(    (M4 - 3*var_s_tay^2) *(Q*Q.')  + ...
                var_s_tay^2 * (Q*ones(Nprime,Nprime)*Q.' + abs(Psi_prime*Psi_prime.').^2 + abs(Psi_prime*Psi_prime').^2 ) +...
                var_s_tay * mu_s_tay^2 * ((Q* ones(Nprime,1)) * abs(w).^2.' + abs(w).^2 * ones(1,Nprime) * Q.' +...
                2 * real( diag(conj(w)) * (Psi_prime * Psi_prime.') * diag(conj(w))) +...
                2 * real( diag(conj(w)) * (Psi_prime * Psi_prime') * diag(w))) +...
                mu_s_tay^4 * abs(w).^2 * abs(w).^2.'  ...
                + var_n*eye(K)) + ...
                - mu_u*mu_u.'; 

g            = c_su/C_uu; %WF vector
g_mean       = (mu_s_tay-t_beta)/t_alpha - c_su*(C_uu\mu_u); %WF scalar
mse_analytic = t_alpha^(-2)*var_s_tay - c_su*(C_uu\c_su'); %Analytic MSE (with sqrt approximation) 
end

