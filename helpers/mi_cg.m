%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2019 Francisco Javier Garcia-Gomez <javier.garcia@tum.de>
% Institute for Communications Engineering (LNT)
% Technical University of Munich, Germany
% www.lnt.ei.tum.de
%
% All rights reserved.
% See licence file LICENSE_MI_CG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [ I, h_Y, h_Y_X ] = mi_cg( x, r, idx_tx, Px )
%[ I ] = mi_cg( s, r )
%   Computes the mutual information between sequences s and r assuming that
%   the channel is conditionally Gaussian, using the method explained in
%   [1]. The sequences s and r have shape DxM, where D is the number of
%   dimensions and M is the number of transmitted symbols. The symbol
%   transmitted at time instant m is s(:, m), and the symbol received at
%   time instant m is s(:, m). The sequence s must come from a discrete
%   memoryless source (DMS) and be long enough, i.e., s must have at most N
%   different columns, with each column appearing many times (50 is usually
%   enough). 
%
%[ I ] = mi_cg( x, r, idx_tx )
%   Computes the mutual information between sequences s=x(:, idx_tx) and r
%   assuming that the channel is conditionally Gaussian. The parameter x is
%   the constellation, containing N columns (constellation points) of size
%   (dimension) D. The parameter idx_tx (with size 1xM) is the sequence of
%   1-based indices of the transmitted symbols, such that the transmitted
%   sequence is s(:, idx_tx). If x and idx_tx are available, this signature
%   is faster than calling mi_cg(x(:, idx_tx), r).
%
%[ I ] = mi_cg( x, r, idx_tx, px )
%   Use this signature to explicitly provide the input probability mass
%   function (pmf) px (of size 1xN), where px(n) is the probability of
%   transmitting the constellation point x(:, n). If px is not provided,
%   the input pmf is assumed to be uniform.
%
% The input sequences s, x, r might be real or complex. If the argument r
% is real, this function assumes real-valued sequences and uses the
% corresponding conditionally Gaussian expressions. If r is complex,
% complex-valued sequences are assumed, and q(y|x) is assumed to be
% circularly-symmetric. If you have a complex, non circularly-symmetric
% channel (such as a channel with phase noise), you need to separate the
% real and imaginary parts to get this function to account for the possible
% correlations between them: 
%      [ I ]=mi_cg([real(x); imag(x)], [real(r); imag(r)], idx_tx, Px)
%
% [1] 
%
% Technische Universitaet Muenchen - Lehrstuhl fuer Nachrichtentechnik
% Date: 12.12.2019
% Author: Francisco Javier Garcia-Gomez <javier.garcia@tum.de>

MAX_MEMORY=200e8; % maximum allowed size for a matrix

MIN_COUNT=2; % minimum number of occurrences of a constellation point to take it into account.

% check if inputs are real or complex
if isreal(r)
    real_factor=2;
else
    real_factor=1;
end

D=size(x, 1);
% if constellation is not provided, obtain it from the data
if nargin<3 || isempty(idx_tx)
    % uniquetol only works with real inputs. Transform to real.
    if real_factor==1
        x=[real(x); imag(x)];
    end
    % this gives the constellation and the transmission indices
    [x, ~, idx_tx]=uniquetol(x.', 'ByRows', true);
    x=x.';
    idx_tx=idx_tx.';
    % if input was complex, transform back to complex
    if real_factor==1
        x=x(1:D, :)+1i*x((D+1):(2*D), :);
    end
end

N=size(x, 2);
M=size(r, 2);

% sort according to transmit index
[idx_tx, idx_sort]=sort(idx_tx);
r=r(:, idx_sort);

% points are sorted according to transmit index (fine for a DMS). This
% means that idx_tx looks like [1,1,...,1,2,2,...,2,..] The variable
% i_bounds will contain the positions where the symbol changes
i_bounds=zeros(1, N+1);

% compute conditional means and covariance matrices
C_n=zeros(D, D, N);
det_n=zeros(1, N);
h_Y_X=0;
for n=1:N
    % find how many times was x(:, n) transmitted and update i_bounds
    N_current_x=find(idx_tx((i_bounds(n)+1):end)==n, 1, 'last');
    if isempty(N_current_x), N_current_x=0; end
    i_bounds(n+1)=i_bounds(n)+N_current_x;
    
    if N_current_x>0
        % Compute mu_n=E[Y|X=x_n] according to Eq. (14) and store it in
        % x(:, n) to save space
        x(:, n)=sum(r(:, (i_bounds(n)+1):i_bounds(n+1)), 2)/(i_bounds(n+1)-i_bounds(n));
        
        % compute C_n=cov[Y|X=x_n] according to Eq. (15)
        C_n(:, :, n)=(r(:, (i_bounds(n)+1):i_bounds(n+1))*r(:, (i_bounds(n)+1):i_bounds(n+1))')/(i_bounds(n+1)-i_bounds(n))-(x(:, n)*x(:, n)');
        % store also the determinant of C(:, :, n)
        det_n(n)=real(det(C_n(:, :, n)));
        
        % add the corresponding contribution to the mutual information (two
        % first lines of Eq. (17)). This, together with
        % D/real_factor*log2(2*pi) in line 87, gives h(Y|X)
        if det_n(n)>0
            h_Y_X=h_Y_X+log2(det_n(n))/real_factor*(i_bounds(n+1)-i_bounds(n))+real(sum(sum(conj(r(:, (i_bounds(n)+1):i_bounds(n+1))-x(:, n)).*(C_n(:, :, n)\(r(:, (i_bounds(n)+1):i_bounds(n+1))-x(:, n))))))/real_factor/log(2);
        end
    end
end
h_Y_X=D/real_factor*log2(2*pi)+h_Y_X/M;

% uniform input pmf Px if not provided
if nargin<4 || isempty(Px)
    Px=repmat(1/N, [1, N]);
end

% remove symbols that were sent too few times
x_counts=diff(i_bounds);
x=x(:, x_counts>=MIN_COUNT);
Px=Px(x_counts>=MIN_COUNT);
Px=Px/sum(Px);
N=length(Px);
C_n=C_n(:, :, x_counts>=MIN_COUNT);
det_n=det_n(x_counts>=MIN_COUNT);

% compute log(py)
logpy=zeros(1, M);
% Here, we might run out of memory. If necessary, we compute log(py) in
% blocks
BLOCK_SIZE=floor(MAX_MEMORY/N);
N_blocks=ceil(M/BLOCK_SIZE);

% loop over blocks of symbols
for i_block=1:N_blocks % this loop can be replaced by parfor to allow parallel computation
    
    % trick to allow parfor: we update logpy_cur instead of logpy, and then
    % add logpy_cur to logpy at the end. Matlab is not clever enough to see
    % that each loop iteration updates different, disjoint blocks of logpy
    logpy_cur=zeros(1, M); 
    
    % beginning of block
    i_start=(i_block-1)*BLOCK_SIZE+1;
    % end of block
    i_end=min(M, i_block*BLOCK_SIZE);
    % block size
    current_block_size=i_end-i_start+1;
    
    % compute exponents of third line of (17)
    exponents=zeros(N, current_block_size);
    for n=1:N
        exponents(n, :)=-log(det_n(n))/real_factor-real(sum(conj(r(:, i_start:i_end)-x(:, n)).*(C_n(:, :, n)\(r(:, i_start:i_end)-x(:, n))), 1))/real_factor;
    end
    
    % compute third line of Eq. (17). We use a custom function (see below)
    % that computes log(sum(exp(x))) avoiding overflow errors
    logpy_cur(i_start:i_end)=math_logsumexp(log(Px(:))+exponents, 1);
    logpy=logpy+logpy_cur;
end

% output entropy h(Y)
h_Y=D/real_factor*log2(2*pi)-mean(logpy)/log(2);

% compute mutual information
I=h_Y-h_Y_X;

end

function [y] = math_logsumexp(x, dim)
%[y] = math_logsumexp(x, dim)
%  Computes log(sum(exp(x), dim)), avoiding overflow errors when one of the
%  x is large.
%
% Technische Universitaet Muenchen - Lehrstuhl fuer Nachrichtentechnik
% Date: 11.12.2019
% Author: Javier Garcia <javier.garcia@tum.de>

if nargin<2 || isempty(dim)
    m=max(x);
    y=m+log(sum(exp(x-m)));
else
    m=max(x, [], dim);
    y=m+log(sum(exp(x-m), dim));
end

end

