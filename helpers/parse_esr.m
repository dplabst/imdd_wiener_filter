% /*********************************************************************
%  * Publication: Wiener Filter for Short-Reach Fiber-Optic Links
%  * Copyright (c) 2020 Daniel Plabst <d.plabst@tum.de>
%  * Institute for Communications Engineering (LNT)
%  * Technical University of Munich, Germany
%  * www.lnt.ei.tum.de 
%  ********************************************************************/

%This function parses the ESR to 'fmincon' in order to find the optimal
%geometric shaping
function [ESR] = parse_esr(Psi_prime, ...% Channel matrix of the dispersive channel
                          idx_Mprime,... % Index in transmit data vector, which is to be estimated (M')
                          var_s_tay,...  % Approximate transmit symbol variance (Taylor series) 
                          mu_s_tay,...   % Approximate transmit symbol mean (Taylor series) 
                          var_n,...      % Noise variance N0*B
                          t_alpha,...    % Taylor series constant 
                          t_beta,...     % Taylor series constant 
                          M4)            % Fourth order central moment with sqrt approximation



%Parse only mse_analytic output for fmincon                       
[~,~, mse_analytic] = wiener_filter(Psi_prime, ...
                                              idx_Mprime,...
                                              var_s_tay,... 
                                              mu_s_tay,...  
                                              var_n,...
                                              t_alpha,...
                                              t_beta,...
                                              M4);

var_s_wo_sqrt = t_alpha.^(-2)* var_s_tay; %Revert to signal variance before sqrt
ESR = mse_analytic/var_s_wo_sqrt; %Define error-to-signal power ratio                                                                                
end

